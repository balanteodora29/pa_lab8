package pa.lab8.compulsory;

import pa.lab8.compulsory.dao.GenresDAO;
import pa.lab8.compulsory.dao.MoviesDAO;
import pa.lab8.compulsory.tables.DataBase;
import pa.lab8.compulsory.tables.Genres;
import pa.lab8.compulsory.tables.Movies;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException {
        DataBase myDB = DataBase.getInstance();

        MoviesDAO moviesDAO = new MoviesDAO();
        Movies movie1 = new Movies("Inside Job", "2010-10-01", 109, 8.2);
        moviesDAO.insert(movie1);

        GenresDAO genresDAO = new GenresDAO();
        Genres genre1 = new Genres("documentary");
        genresDAO.insert(genre1);
    }
}
