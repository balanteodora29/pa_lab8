package pa.lab8.compulsory.dao;

import pa.lab8.compulsory.tables.DataBase;
import pa.lab8.compulsory.tables.Movies;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MoviesDAO implements DAO<Movies> {

    List<Movies> moviesList;
    static Connection conn = DataBase.getConnection();
    private DataBase db;


    public MoviesDAO() throws SQLException {
        db = DataBase.getInstance();
        moviesList = db.getMovies();
    }


    @Override
    public void insert(Movies movie) throws SQLException {
        moviesList.add(movie);
        String query = "INSERT INTO movies_manager.movies (title,release_date,duration,score) VALUES (?,?,?,?)";
        PreparedStatement statement = conn.prepareStatement(query);
        statement.setString(1, movie.getTitle());
        statement.setString(2, movie.getReleaseDate());
        statement.setInt(3, movie.getDuration());
        statement.setDouble(4, movie.getScore());
        statement.executeUpdate();
    }

    @Override
    public List<Movies> findById(int id) throws SQLException {
        List<Movies> pickedMovies = new ArrayList<>();
        for(Movies m : moviesList) {
            if(m.getId() == id)
                pickedMovies.add(m);
        }
        return pickedMovies;
    }

    @Override
    public List<Movies> findByName(String name) {
        List<Movies> pickedMovies = new ArrayList<>();
        for(Movies m : moviesList) {
            if(m.getTitle().equals(name))
                pickedMovies.add(m);
        }
        return pickedMovies;
    }

}
