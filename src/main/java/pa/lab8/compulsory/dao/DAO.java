package pa.lab8.compulsory.dao;

import java.sql.SQLException;
import java.util.List;

public interface DAO<T> {
    void insert(T t) throws SQLException;
    List<T> findById(int id) throws SQLException;
    List<T> findByName(String name)  throws SQLException;
}
