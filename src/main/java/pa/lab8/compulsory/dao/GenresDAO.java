package pa.lab8.compulsory.dao;

import pa.lab8.compulsory.tables.DataBase;
import pa.lab8.compulsory.tables.Genres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GenresDAO implements DAO<Genres> {
    List<Genres> genresList;
    static Connection conn = DataBase.getConnection();
    private DataBase db;

    public GenresDAO() throws SQLException {
        db = DataBase.getInstance();
        genresList = db.getGenres();
    }

    @Override
    public void insert(Genres genre) throws SQLException {
        genresList.add(genre);
        String query = "INSERT INTO movies_manager.genres (name) VALUES (?)";
        PreparedStatement statement = conn.prepareStatement(query);
        statement.setString(1,genre.getName());
        statement.executeUpdate();
    }

    @Override
    public List<Genres> findById(int id) throws SQLException {
        List<Genres> genres = new ArrayList<>();
        for(Genres g : genresList)
            if(g.getId() == id)
                genres.add(g);
        return genres;
    }

    @Override
    public List<Genres> findByName(String name) {
        List<Genres> genres = new ArrayList<>();
        for(Genres g : genresList)
            if(g.getName().equals(name))
                genres.add(g);
        return genres;
    }
}
