package pa.lab8.compulsory.tables;

import java.sql.SQLException;

public class Movies {
    private int id;
    private String title;
    private String release_date;
    private int duration;
    private double score;

    public Movies() {}

    public Movies(String title, String release_date, int duration, double score) {
        this.title = title;
        this.release_date = release_date;
        this.duration = duration;
        this.score = score;
    }

    public int getId() throws SQLException {
        return DataBase.getIdMovie(this.title);
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getReleaseDate() {
        return release_date;
    }

    public void setReleaseDate(String release_date) {
        this.release_date = release_date;
    }

    public double getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Movies{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", release_date='" + release_date + '\'' +
                ", duration=" + duration +
                ", score=" + score +
                '}';
    }

}
