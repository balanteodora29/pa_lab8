package pa.lab8.compulsory.tables;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataBase {
    private static Connection conn = null;
    private static DataBase single_instance = null;
    static final String DB_URL = "jdbc:mysql://localhost/";
    static final String USER = "root";
    static final String PASS = "root";

    public DataBase() {
        try {
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    public static Connection getConnection() {
        return conn;
    }

    public static DataBase getInstance() {
        if(single_instance == null)
            single_instance = new DataBase();
        return single_instance;
    }

    public List<Movies> getMovies() throws SQLException {
        List<Movies> movies = new ArrayList<>();
        String sql = "SELECT * FROM movies_manager.movies";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery(sql);
        while (rs.next()) {
            Movies movie = new Movies();
            movie.setTitle(rs.getString("title"));
            movie.setReleaseDate(rs.getString("release_date"));
            movie.setDuration(rs.getInt("duration"));
            movie.setScore(rs.getInt("score"));
            movies.add(movie);
        }
        return movies;
    }

    public List<Genres> getGenres() throws SQLException {
        List<Genres> genres = new ArrayList<>();
        String sql = "SELECT * from movies_manager.genres";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery(sql);
        while(rs.next()) {
            Genres genre = new Genres();
            genre.setName(rs.getString("name"));
            genres.add(genre);
        }
        return genres;
    }

    public static int getIdMovie(String movieTitle) throws SQLException {
        String sql = "SELECT id from movies where title=" + movieTitle + ";";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery(sql);
        int id = rs.getInt(1);
        return id;
    }

    public static int getIdGenre(String genreName) throws SQLException {
        String sql = "SELECT id from genres where name=" + genreName + ";";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery(sql);
        int id = rs.getInt(1);
        return id;
    }

}
