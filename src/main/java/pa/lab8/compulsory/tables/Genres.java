package pa.lab8.compulsory.tables;

import java.sql.SQLException;

public class Genres {
    private int id;
    private String name;

    public Genres () { }

    public Genres(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() throws SQLException {
        return DataBase.getIdGenre(this.name);
    }

    @Override
    public String toString() {
        return "Genres{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
